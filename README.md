Jynx
====

This is a Java-based web browser that was originally intended to be in the spirit of Lynx, producing a web page on the command line. That goal has been abandoned in favor of providing a windowed browser, instead. The current production track is focused on providing a faster browsing experience than the four mainline browsers, while still being useful as a daily browser. In addition to this, because the browser is written in Java, it provides an installable program for any platform that can run the Java Runtime Environment. This browser will run on your wristwatch if it has the JRE.

At the moment, Jynx doesn't parse the HTML; it just prints it straight to the console. The plan is to eventually use the Webkit libraries to parse HTML and CSS. The name Jynx stands for "Jon's Lynx", or alternatively, "Java Lynx". I've decided not to change the name of the project, even though it's not a text-based browser anymore. Frankly, I think the name sounds catchy. Pull requests are welcome, and all feature requests are considered.

######Usage######

    $ git clone git@github.com:jonlandrum/jynx.git
    $ cd jynx
    $ javac jynx.java
    $ java jynx

######Screenshot of an example run######

![A screenshot of some sample output](screenshot.png "It only vomits the code to the terminal at the moment")
